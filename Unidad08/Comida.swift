//
//  Comida.swift
//  Unidad08
//
//  Created by Macbook Pro on 9/17/17.
//  Copyright © 2017 Macbook Pro. All rights reserved.
//

import Foundation

struct Comida {
    let formatter = DateFormatter()
    var hora : Date
    var descripcion: String
    var creditos: Double
    
    init (hora: Date, descripcion: String, creditos: Double) {
        self.hora = hora
        self.descripcion = descripcion
        self.creditos = creditos
        formatter.timeStyle = .short

    }
    
    var title: String {
        get {
            return "\(formatter.string(from: self.hora)) - \(self.descripcion)"
        }
    }
}
