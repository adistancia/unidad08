//
//  ComidasTableViewController.swift
//  Unidad08
//
//  Created by Macbook Pro on 9/17/17.
//  Copyright © 2017 Macbook Pro. All rights reserved.
//

import UIKit

/*
 * Funcion que nos permite construir una fecha pasandole hora, minutos y segundos.
 */
func timeHelper(hour: Int = 0, minute: Int = 0, seconds: Int = 0, day: Date = Date()) -> Date {
    let gregorian = Calendar(identifier: .gregorian)
    var components = gregorian.dateComponents([.year, .month, .day, .hour, .minute, .second], from: day)
    
    components.hour = hour
    components.minute = minute
    components.second = seconds
    
    return gregorian.date(from: components)!
}

class ComidasTableViewController: UITableViewController {

    let cellIdentifier = "cell"
    
    let comidas = [
        Comida(hora: timeHelper(hour:  9), descripcion: "Cafe con leche con tostadas", creditos: 5.0),
        Comida(hora: timeHelper(hour: 11), descripcion: "Manzana", creditos: 2),
        Comida(hora: timeHelper(hour: 13), descripcion: "Bife con ensalada", creditos: 8.0),
        Comida(hora: timeHelper(hour: 15), descripcion: "Yogur con cereales", creditos: 6.0),
        Comida(hora: timeHelper(hour: 19), descripcion: "2 Pintas de Cerveza con papas con chedar y panceta", creditos: 24.0)
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.contentInset.top = UIApplication.shared.statusBarFrame.height

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.comidas.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath)
        
        cell.textLabel?.text = self.comidas[indexPath.row].title
        
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            if  let comidaViewController = segue.destination as? ComidaViewController {
                comidaViewController.comida = comidas[selectedIndexPath.row]
            }
        }
    }

}
