//
//  ComidaViewController.swift
//  Unidad08
//
//  Created by Macbook Pro on 9/17/17.
//  Copyright © 2017 Macbook Pro. All rights reserved.
//

import UIKit

class ComidaViewController: UIViewController {

   
    var comida: Comida?
    
    @IBOutlet weak var horaDatePicker: UIDatePicker!
    @IBOutlet weak var creditosTextView: UITextField!
    @IBOutlet weak var descripcionTextView: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()
        if let comida = self.comida {
            horaDatePicker.date = comida.hora
            descripcionTextView.text = comida.descripcion
            creditosTextView.text = "\(comida.creditos)"
            navigationItem.title = comida.descripcion
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

